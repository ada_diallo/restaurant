
import { BrowserRouter, Routes, Route } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
import "./Components/Utiles.css"
import Header from "./Components/Header";
import LandingPage from "./Components/LandinPage";
import About from "./Components/About";
import Menu from "./Components/Menu";
import FoodMenu from "./Components/FoodMenu";
import ButtonMenu from "./Components/ButtonMenu";
import HappyClient from "./Components/HappyClient";
import ReserveTable from "./Components/ReserveTable";
import Contact from "./Components/Contact";
import Navbar from "./Components/Navbar";
import Footer from "./Components/Footer";
import TitleMenu from "./Components/TitleMenu";
import Navbar2 from "./Components/Active";
import HeaderSlide from "./Components/HeaderSlide";
import MenuSlide from "./Components/MenuSlide";
import BasicExample from "./Components/Active";

function App() {
  return (
    <div className="App " style={{overflow:"hidden"}}>
      <BrowserRouter>
        <Routes>
        <Route path="/" element={<LandingPage/>} />
        <Route path="/navbar" element={<Navbar/>} />

          <Route path="/header" element={<Header />} />
          <Route path="/contact" element={<Contact/>} />

          <Route path="/about" element={<About/>} />
          <Route path="/menu" element={<Menu/>} />
          <Route path="/food-menu" element={<FoodMenu/>} />
          <Route path="/btn-menu" element={<ButtonMenu/>} />
          <Route path="/happy-client" element={<HappyClient/>} />

          <Route path="/table" element={<ReserveTable/>} />
          <Route path="/footer" element={<Footer/>} />
          <Route path="/nav2" element={<BasicExample/>} />
          <Route path="/slide-header" element={<HeaderSlide/>} />
          <Route path="/slide-menu" element={<MenuSlide/>} />







         
        </Routes>
      </BrowserRouter>
    </div>
  );
}
export default App;