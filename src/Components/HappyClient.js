// src/components/bootstrap-carousel.component.js
import React from "react";
import client2 from "./Images/fehrat-bulbul-est-heureux-d-accueillir-a-nouveau-ses-clients-1635785519.jpg"
import client3 from "./Images/Sunday_restaurant_0493-1-scaled-e1643829458405.jpg"
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from "react-responsive-carousel";
import {AiFillStar} from "react-icons/ai"

export default function HappyClient() {
  return (
    <div>
      <div className="container-fluid contain-happyClient  d-flex justify-content-center align-items-center">
        <div className="container ">
          
          <Carousel
      showIndicators={false}
        showArrows={true}
        infiniteLoop={true}
        showThumbs={false}
        showStatus={false}
        autoPlay={true}
        interval={6100}
      >
        
        <div>
          <div className="myCarousel">
            <h3 className="h3 text-white">Client(es) Heureux(se)</h3>

            <p className="">
              Lorem Ipsum is simply dummy text used in typesetting and layout
              before printing. Lorem Ipsum has been the printing
              <br />
              industry's standard dummy text since the 1500s, when an anonymous
              printer pieced together
              <br /> chunks of text to make a specimen book of typefaces.
            </p>
          </div>
          <img src={client2} />
          <h4 className="name-client mt-3">Mamadou Bah</h4>
          <div className="icone-start">
          <AiFillStar color="#ffc23c"/>
          <AiFillStar color="#ffc23c"/>
          <AiFillStar color="#ffc23c"/>
          <AiFillStar color="#ffc23c"/>
          <AiFillStar color="#ffc23c"/>
          </div>
         







        </div>


        <div>
          <div className="myCarousel">
            <h3 className="h3 text-white">Client(es) Heureux(se)</h3>

            <p className="">
              Lorem Ipsum is simply dummy text used in typesetting and layout
              before printing. Lorem Ipsum has been the printing
              <br />
              industry's standard dummy text since the 1500s, when an anonymous
              printer pieced together
              <br /> chunks of text to make a specimen book of typefaces.
            </p>
          </div>
          <img src={client3} />
          <h4 className="name-client mt-3">Halima Ndiaye</h4>
          <div className="icone-start">
          <AiFillStar color="#ffc23c"/>
          <AiFillStar color="#ffc23c"/>
          <AiFillStar color="#ffc23c"/>
          <AiFillStar color="#ffc23c"/>
          <AiFillStar color="#ffc23c"/>
          </div>

        </div>

        
        <div className="">
          <div className="myCarousel">
            <h3 className="h3 text-white">Client(es) Heureux(se)</h3>

            <p className="">
              Lorem Ipsum is simply dummy text used in typesetting and layout
              before printing. Lorem Ipsum has been the printing
              <br />
              industry's standard dummy text since the 1500s, when an anonymous
              printer pieced together
              <br /> chunks of text to make a specimen book of typefaces.
            </p>
          </div>
          <img src={client3} />
          <h4 className="name-client mt-3">Ami Faye</h4>
          <div className="icone-start">
          <AiFillStar color="#ffc23c"/>
          <AiFillStar color="#ffc23c"/>
          <AiFillStar color="#ffc23c"/>
          <AiFillStar color="#ffc23c"/>
          <AiFillStar color="#ffc23c"/>
          </div>

        </div>
      </Carousel>
        </div>
      </div>
    </div>
  );
}
