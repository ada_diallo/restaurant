import React from "react";
import image from "./Images/Cuisine-sénégalaise-thieboudieune-bou-wekh-removebg-preview.png"
export default function Header () {
    return(
        <div className="container-fluid p-0  header d-flex justify-content-center align-items-center  ">
           <div className="container pt-5 zoom">
            <div className="row d-flex justify-content-center align-items-center">
                <div className="col-lg-6 line">
                <p className="header-text text-white">
                Testez votre plat préféré<br/>
                <span className="food">chez  CHEF FOOD</span>
            </p>
       
            {/* <div className="mt-3"> */}
            {/* </div> */}
                </div>
        <div className="col-lg-6  header-text">
                <img src={image} className='image img-fluid'/>
        </div>
           

            </div>
            </div> 
        </div>
    )
}