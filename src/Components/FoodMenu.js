import React from "react";
import ButtonMenu from "./ButtonMenu";
import Menu from "./Menu";

export default function FoodMenu() {
  return (
    <div className="container-fluid  contain-foodMenu   ">
      <div className="container  d-flex justify-content-center align-items-center">
        <div className="container" id="food-menu">
          <h3 className="h3 text-center pt-3">Le Menu</h3>
          <div className="row  ">
            <div className="col-lg-6 text-center p-5">
              <div className="d-flex list-menu">
                <div className="col-lg">
                  <p className="text-uppercase p-menu text-center ">
                    Bolognaiise
                    <br />{" "}
                    <span className="span-menu text-muted">
                      Tomate,mozzarella,champignons,oignons
                    </span>
                  </p>
                </div>

                <div className="col-lg prix d-flex justify-content-end">
                  3500 FCFA
                </div>
              </div>
              <div className="">
                <hr />
              </div>
              <div className="d-flex list-menu">
                <div className="col-lg">
                  <p className="text-uppercase p-menu ">
                    Bolognaiise
                    <br />
                    <span className="span-menu text-muted">
                      Tomate,mozzarella,champignons,oignons
                    </span>
                  </p>
                </div>

                <div className="col-lg prix d-flex justify-content-end">
                  4000 FCFA
                </div>
              </div>
              <div className="">
                <hr />
              </div>

              <div className="d-flex list-menu">
                <div className="col-lg">
                  <p className="text-uppercase p-menu ">
                    Végétarienne
                    <br />{" "}
                    <span className="span-menu text-muted">
                      Tomate,mozzarella,champignons,oignons
                    </span>
                  </p>
                </div>

                <div className="col-lg prix d-flex justify-content-end ">
                  2500 FCFA
                </div>
              </div>
              <div className="">
                <hr />
              </div>

              <div className="d-flex list-menu">
                <div className="col-lg">
                  <p className="text-uppercase p-menu ">
                    Double cheese
                    <br />
                    <span className="span-menu text-muted">
                      Tomate,mozzarella,champignons,oignons
                    </span>
                  </p>
                </div>

                <div className="col-lg prix d-flex justify-content-end ">
                  3500 FCFA
                </div>
              </div>
            </div>
            <div className="col-lg-6 text-center d-flex flex-column p-5">
              <div className="d-flex list-menu">
                <div className="col-lg">
                  <p className="text-uppercase p-menu ">
                    Margeritha
                    <br />{" "}
                    <span className="span-menu text-muted">
                      Tomate,mozzarella,champignons,oignons
                    </span>
                  </p>
                </div>

                <div className="col-lg prix  d-flex justify-content-end">
                  3500 FCFA
                </div>
              </div>
              <div className="">
                <hr />
              </div>

              <div className="d-flex list-menu">
                <div className="col-lg">
                  <p className="text-uppercase p-menu ">
                    Bolognaiise
                    <br />{" "}
                    <span className="span-menu text-muted">
                      Tomate,mozzarella,champignons,oignons
                    </span>
                  </p>
                </div>

                <div className="col-lg prix d-flex justify-content-end">
                  4000 FCFA
                </div>
              </div>
              <div className="">
                <hr />
              </div>

              <div className="d-flex list-menu">
                <div className="col-lg">
                  <p className="text-uppercase p-menu ">
                    Végétarienne
                    <br />{" "}
                    <span className="span-menu text-muted">
                      Tomate,mozzarella,champignons,oignons
                    </span>
                  </p>
                </div>

                <div className="col-lg prix d-flex justify-content-end ">
                  2500 FCFA
                </div>
              </div>
              <div className="">
                <hr />
              </div>

              <div className="d-flex list-menu">
                <div className="col-lg">
                  <p className="text-uppercase p-menu ">
                    Double cheese
                    <br />{" "}
                    <span className="span-menu text-muted">
                      Tomate,mozzarella,champignons,oignons
                    </span>
                  </p>
                </div>

                <div className="col-lg prix d-flex justify-content-end">
                  3500 FCFA
                </div>
              </div>
            </div>
          </div>
          <div className="button ">
            <ButtonMenu />
          </div>
        </div>
      </div>
    </div>
  );
}
