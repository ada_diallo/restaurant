import React from "react";
import About from "./About";
import Contact from "./Contact";
import FoodMenu from "./FoodMenu";
import Footer from "./Footer";
import HappyClient from "./HappyClient";
import Menu from "./Menu";
import ReserveTable from "./ReserveTable";
import TitleMenu from "./TitleMenu";
import HeaderSlide from "./HeaderSlide";
import Navbar1 from "./Navbar";





export default function LandingPage(){

    return(
        <div className=" contain-page">
         <div className="row">
                <div className="col-lg-12">
                <Navbar1/>
                {/* <Navbar3 /> */}


                </div>
            </div>
            <div className="row ">
            <div className="col-lg-12  ">

                <HeaderSlide/>
            </div>
            </div>
            <div className="row section-contact">
            <div className="col-lg-12">

                <Contact/>
            </div>
         </div>
           
          
         <div className="row section-contact">
            <div className="col-lg-12">

                <About/>
            </div>
         </div>
             <div className="row">
                <div className=" col-lg-12 bg-dark ">
                <TitleMenu/>

                </div>
            </div>
            <div className="row">

                <div className=" col-lg-12">
                <Menu/>

                </div>
            </div>
            <div className="row">
                <div className="col-lg-12">
                <FoodMenu/>

                </div>
                </div>
                <div className="row pt-5">
                <div className="col-lg-12">
                <HappyClient/>

                </div>
                </div>
            <div className="row">
                <div className="col-lg-12">
                <ReserveTable/>

                </div>
            </div>
            <div className="row">
                <div className="col-lg-12">
                <Footer/>

                </div>
            </div> 
            {/* <div className="foodmenu">
            </div> */}
            {/* <div className="">
                <TitleMenu/>
            </div>
          
            <div className="">
                <HappyClient/>
            </div>
            <div className="">
                <ReserveTable/>
            </div>
            <div className="">
                <Footer/>
            </div> */}
        
                </div>
    )
}