import React from "react";
import image from "./Images/gallery-buddha-bowl-01-removebg-preview.png";
export default function Header2() {
  return (
    <div className="container-fluid p-0 bg-dark header d-flex justify-content-center align-items-center  ">
      <div className="container pt-5 zoom">
        <div className="row d-flex justify-content-center align-items-center">
          <div className="col-lg-6">
            <p className="header-text text-white">
              Testez votre plat préféré
              <br />
              <span className="food">chez CHEF FOOD</span>
            </p>
          </div>
          <div className="col-lg-6 header-text">
            <img src={image} className="image img-fluid" />
          </div>
        </div>
      </div>
    </div>
  );
}
