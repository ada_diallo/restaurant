import React from "react";
import table from "./Images/table.avif";
import { BsPerson } from "react-icons/bs";
import { CgCalendarDates } from "react-icons/cg";
import { IoMdTime } from "react-icons/io";
import { MdOutlineMailOutline } from "react-icons/md";
import { MdSupervisorAccount } from "react-icons/md";
import { BsPhone } from "react-icons/bs";
import { Link } from "react-router-dom";
























export default function ReserveTable() {





  return (
    <div className="container-fluid     d-flex justify-content-center align-items-center contain-table" >
      <div className="container " id="table">
        <h3 className=" text-center h3 pt-3">Reserver votre table</h3>

        <div className="row col-img mt-5 row-table ">
          <div className="col-lg text-center">
            {/* <div className="card card-table"> */}
            <img src={table} className="img-fluid img-table" />
            <div class="card-body"></div>
            {/* </div> */}
          </div>
          <div className="col-lg  ">
            <form className="form ">
              <div className="row m-4 d-flex  justify-content-center align-items-center">
                <div className="col-lg content  ">
                  <div className="champ">
                    <input
                      className="input"
                      id="form-one"
                      type="text"
                      placeholder="Name*"
                      required
                    />
                    <BsPerson className="icone" size={30} />

                    <span className="border"></span>
                  </div>
                </div>
                <div className="col-lg content ">
                  <div className="champ">
                    <input
                      className="input"
                      id="form-one"
                      type="text"
                      placeholder="Date*"
                      required
                    />
                    <CgCalendarDates className="icone" size={30} />
                    <span className="border"></span>
                  </div>
                </div>
              </div>
              <div className="row m-4 d-flex justify-content-center align-items-center">
                <div className="col-lg content ">
                  <div className="champ">
                    <input
                      className="input"
                      id="form-one"
                      type="text"
                      placeholder="Heure*"
                      required
                    />
                    <IoMdTime className="icone" size={30} />
                    <span className="border"></span>
                  </div>
                </div>
                <div className="col-lg content ">
                  <div className="champ">
                    <input
                      className="input"
                      id="form-one"
                      type="mail"
                      placeholder="Email*"
                      required
                    />
                    <MdOutlineMailOutline className="icone" size={30} />
                    <span className="border"></span>
                  </div>
                </div>
              </div>
              <div className="row m-4 d-flex  justify-content-center align-items-center ">
                <div className="col-lg content ">
                  <div className="champ">
                    <input
                      className="input"
                      id="form-one"
                      type="number"
                      placeholder="Invité(és)*"
                      required
                    />
                    <MdSupervisorAccount className="icone" size={30} />
                    <span className="border"></span>
                  </div>
                </div>
                <div className="col-lg content ">
                  <div className="champ">
                    <input
                      className="input"
                      id="form-one"
                      type="tel"
                      placeholder="Téléphone*"
                      required
                    />
                    <BsPhone className="icone" size={30} />
                    <span className="border"></span>
                  </div>
                </div>
              </div>
            </form>
            <div className="row">
              <div className="col-lg d-flex justify-content-center align-items-center">
              <Link className="link" to="/#"><button className="btn-footer">Réserver</button></Link>

              </div>
              <div className="col-lg  d-flex justify-content-center align-items-center">
                    <p className="text-uppercase p-menu ">
                    pour vos  livraison 
                      <br /> <span className="span"> à domicile appelez</span>
                    </p>
                  </div>
                  <div className="col-lg  d-flex justify-content-center align-items-center">
              <p className="text-uppercase tel">
                  00221-78-145-26-67
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
