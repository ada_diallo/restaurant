import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import React, { useState, useEffect } from "react";
import logo from "./Images/logo-removebg-preview.png";

import NavDropdown from 'react-bootstrap/NavDropdown';

export default function Navbar1() {
  
    const [navColor, setnavColor] = useState("transparent");
  const listenScrollEvent = () => {
    window.scrollY > 108? setnavColor("#fa2fb5") : setnavColor("#fa2fb5");
  };
  useEffect(() => {
    window.addEventListener("scroll", listenScrollEvent);
    return () => {
      window.removeEventListener("scroll", listenScrollEvent);
    };
  }, []);
  return (
    <Navbar collapseOnSelect expand="lg"  fixed="top" 
    style={{background:
      navColor}}
    >
      <Container>
        
        <Navbar.Brand href="#home">
        <a
href="/"
className="brand-name text-white d-flex justfy-content-center align-items-center"
>
<img src={logo} className="img-fluid logo" />
</a>
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav"  className='hamburger'/>
        <Navbar.Collapse id="responsive-navbar-nav " className='nav-menu'>
          <Nav className=" nav">
            {/* <li href="" className='active'>Accueil</li> */}
            <ul>
          
              <li>
              <Nav.Link  href="">
                Accueil
              </Nav.Link>{" "}              </li>
              <li>
              <Nav.Link  href="#contact">
                Contact
              </Nav.Link>{" "}              </li>

              <li>
              <Nav.Link  href="#about"
              >
                About
              </Nav.Link>{" "}              </li>
              <li>
              <Nav.Link href="#menu">
                Menu
              </Nav.Link>{" "}              </li>

              <li>
              <Nav.Link   href="#food-menu">
                Recette
              </Nav.Link>{" "}              </li>
              <li>
              <Nav.Link href="#table">
                Réservation
              </Nav.Link>{" "}              </li>
          </ul>

           
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

