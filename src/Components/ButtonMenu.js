import React from "react";
import { Link } from "react-router-dom";

export default  function ButtonMenu () {
    return(
        <div className="container d-flex justify-content-center align-items-center">
            <div className="row">
                <div className="col-lg-12">
 <Link to="/#" className="link-btn">
            <button className=" mt-5 text-uppercase btn-menu">voir le menu</button>

            </Link>
                </div>
            </div>
           
        </div>
    )
}