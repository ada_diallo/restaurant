import React, { useState} from "react";
import { Link } from "react-scroll";
import logo from "./Images/logo-removebg-preview.png";

export default function Navbar3() {
  const [isNavExpanded, setIsNavExpanded] = useState(false);

  return (
    <div className="container-fluid  ">
      <nav
        class=" navigation navbar  navbar-expand-lg navbar-light fixed-top "
        id="mainNav"
      >
        <a
          href="/"
          className="brand-name text-white d-flex justfy-content-center align-items-center"
        >
          <img src={logo} className="img-fluid logo" />
        </a>
        <button
          className="hamburger"
          onClick={() => {
            setIsNavExpanded(!isNavExpanded);
          }}
        >
          {" "}
          {/* icon from heroicons.com */}
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="h-5 w-5"
            viewBox="0 0 20 20"
            fill="white"
          >
            <path
              fillRule="evenodd"
              d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM9 15a1 1 0 011-1h6a1 1 0 110 2h-6a1 1 0 01-1-1z"
              clipRule="evenodd"
            />
          </svg>
        </button>
        <div
          className={
            isNavExpanded ? "navigation-menu expanded" : "navigation-menu"
          }
        >
          <ul>
            <li>
              <Link activeClass="active" smooth spy to="">
                Accueil
              </Link>{" "}
            </li>
            <li>
              <Link activeClass="active" smooth spy to="contact">
                Contact
              </Link>{" "}
            </li>

            <li>
              <Link activeClass="active" smooth spy to="about">
                About
              </Link>{" "}
            </li>
            <li>
              <Link activeClass="active" smooth spy to="menu">
                Menu
              </Link>{" "}
            </li>

            <li>
              <Link activeClass="active" smooth spy to="food-menu">
                Recette
              </Link>{" "}
            </li>
            <li>
              <Link activeClass="active" smooth spy to="table">
                Réservation
              </Link>{" "}
            </li>
          </ul>
        </div>
      </nav>
    </div>
  );
}
