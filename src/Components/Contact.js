import React from "react";
import book from "./Images/083e4d106233059.5f8b377bd4286-removebg-preview.png";
import { TiMessages } from "react-icons/ti";
import { MdOutlineNavigateNext } from "react-icons/md";
import { FiPhoneCall } from "react-icons/fi";
import About from "./About";

export default function Contact() {
  return (
    <div className="container-fluid contact justify-content-center align-items-center container-contact">
      <div
        className=" container bg-light d-flex   "
        id="contact"
      >
        <div className="row">
         <div className="col-lg-12">
         <div className="row">
          <div className="col-lg">
            <img src={book} className="book-image img-fluid" />
          </div>
          <div className="col-lg d-flex align-items-center">
            <div className="">
              <TiMessages size={30} />
            </div>

            <p className="text-uppercase red">
              red <br />
              <span className="reviews"> reviews</span>
            </p>
            <div className="next-icone d-flex justify-content-center align-items-center">
              <MdOutlineNavigateNext size={20} />
            </div>
          </div>
          <div className="col-lg d-flex align-items-center">
            <div className="">
              <FiPhoneCall size={30} />
            </div>

            <p className="text-uppercase red">
              <span className="reviews"> livraison a domicile </span>
            </p>
          </div>
          <div className="col-lg d-flex align-items-center">
            <p className="text-uppercase red">00221-78-145-26-67</p>
          </div>
        </div>
          </div> 
        </div>
       
      </div>
      {/* <div className="mt-5">
        <About />
      </div> */}
    </div>
  );
}
