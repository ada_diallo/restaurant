import React from "react";
import Scrollspy from "react-scrollspy";
import { FaFacebookF } from "react-icons/fa";
import { AiOutlineTwitter } from "react-icons/ai";
import { AiOutlineGooglePlus } from "react-icons/ai";
import app from "./Images/available-the-app-store-badge-vector-11.png";
import { BsApple } from "react-icons/bs";
export default function Footer() {
  return (
    <div className="container-fluid bg-dark  d-flex  justify-content-center align-items-center">
      <div className="container footer  d-flex flex-column  justify-content-center">
        <div className="row ">
          <div className="col-lg-4 col1">
            <h3 className="footer-title">Navigation</h3>
            <ul className="footer-navigation d-flex justify-content-center align-items-center">
              <Scrollspy
                items={["about", "food-menu", "menu", "contact"]}
                currentClassName="is-current"
                style={{ fontSize: 18 }}
              >
                <li>
                  <a href="/">Accueil</a>
                </li>
                <li>
                  <a href="/#food-menu">Menu</a>
                </li>
                <li className="nav-item active">
                  {" "}
                  <a href="/#about"> Blog</a>
                </li>

                <li>
                  <a href="/#table">Reservation</a>
                </li>
                <li>
                  <a href="/#menu">Recette</a>
                </li>
                <li>
                  <a href="/#contact">Contact</a>
                </li>
              </Scrollspy>
            </ul>
          </div>
          <div className="col-lg-4  col2">
            <div className=" ">
              <h3 className="footer-title">Plus d'information</h3>
              <p className=" text-muted">
                Entrer votre address email pour ne rater aucune information
              </p>
            </div>

            <div className="row">
              <div className="col-lg form-input">
                <input type="email" placeholder="Address email" />
              </div>
              <div className="col-lg mt-3">
                <button className="btn-footer">S'abonner</button>
              </div>
            </div>

            <div className="mt-3 icons d-flex  justify-content-center align-items-center -4">
              <div className="mx-2 facebook d-flex justify-content-center align-items-center">
                <FaFacebookF size={20} color="#fff" />
              </div>
              <div className="twitter d-flex justify-content-center align-items-center mx-2">
                <AiOutlineTwitter color="#fff" size={30} className="" />
              </div>
              <div className=" mx-2 google-plus d-flex justify-content-center align-items-center">
                <AiOutlineGooglePlus color="#fff" size={30} />
              </div>
            </div>
          </div>
          <div className="col-lg-4 ">
            <h3 className="footer-title">Notre application est disponible:</h3>
            <div className="d-flex flex-column  justify-content-center align-items-center m-0">
              <img src={app} className="img-fluid app" />
              <img src={app} className="img-fluid app" />
            </div>
          </div>
        </div>
        {/* <div className="row">

                <div className="col-lg-12">

                <h1 class="hr mt-5">Copyright © 2021 <span>Chef Food</span>. tous droits réservés.

</h1>
                </div>
            </div> */}
      </div>
    </div>
  );
}
