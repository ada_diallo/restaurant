import React from "react";
import pizza from "./Images/slade.webp";
import image from "./Images/soupe-kandja.jpg";
import FoodMenu from "./FoodMenu";
import guinar from "./Images/guinar-thiep-removebg-preview.png"
import bolognaise from "./Images/bolognaise-removebg-preview.png"
export default function Menu() {
  return (
    <div className="container-fluid contain-menu d-flex  justify-content-center align-items-center">
      <div className="container  d-flex   justify-content-center header-text " id="menu">
        <div className="row mt-5 ">
          <div className="col-lg-3 cardMenu1">
            <div className="card card-menu w-100 d-flex justify-content-center align-items-center">
              <div className="card-body ">
                <div className="card-img contain zoom">
                <img src={bolognaise} className="img-menu img-fluid" />
                  <div className="overlay">
                    <div className="icon">
                      <p className="fa fa-user">3500 FCFA</p>
                    </div>
                  </div>
                </div>
                <div className="">
                  <h3 className="h3 text-center pt-3">Soupe Kandja</h3>
                  <p className="p pt-2">
                    Lorem Ipsum is simply dummy text used in typesetting and
                    layout before printing. Lorem Ipsum has been the printing
                    industry's standard dummy text since the 1500s, when an
                    anonymous printer pieced together chunks of text to make a
                    specimen book of typefaces.
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div className=" col-lg-3">
            <div className="card card-menu w-100 d-flex justify-content-center align-items-center">
              <div className="card-body ">
                <div className="card-img contain zoom">
                <img src={bolognaise} className="img-menu img-fluid" />

                  <div className="overlay">
                    <div className="icon">
                      <p className="fa fa-user">2500 FCFA</p>
                    </div>
                  </div>
                </div>
                <div className="">
                  <h3 className="h3 text-center pt-3">Salade lyonnaise</h3>
                  <p className="p pt-2">
                    Lorem Ipsum is simply dummy text used in typesetting and
                    layout before printing. Lorem Ipsum has been the printing
                    industry's standard dummy text since the 1500s, when an
                    anonymous printer pieced together chunks of text to make a
                    specimen book of typefaces.
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-3">
            <div className="card card-menu w-100 d-flex justify-content-center align-items-center">
              <div className="card-body ">
                <div className="card-img contain zoom">
                <img src={bolognaise} className="img-menu img-fluid" />
                  <div className="overlay">
                    <div className="icon">
                      <p className="fa fa-user">3000 FCFA</p>
                    </div>
                  </div>
                </div>

                <div className="">
                  <h3 className="h3 text-center pt-3">Salade lyonnaise</h3>
                  <p className="p pt-2">
                    Lorem Ipsum is simply dummy text used in typesetting and
                    layout before printing. Lorem Ipsum has been the printing
                    industry's standard dummy text since the 1500s, when an
                    anonymous printer pieced together chunks of text to make a
                    specimen book of typefaces.
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-3">
            <div className="card card-menu w-100 d-flex justify-content-center align-items-center">
              <div className="card-body ">
                <div className="card-img contain zoom ">
                  <img src={bolognaise} className="img-menu img-fluid" />
                  <div className="overlay">
                    <div className="icon">
                      <p className="fa fa-user">4500 FCFA</p>
                    </div>
                  </div>
                </div>

                <div className="">
                  <h3 className="h3 text-center pt-3">Salade lyonnaise</h3>
                  <p className="p pt-2">
                    Lorem Ipsum is simply dummy text used in typesetting and
                    layout before printing. Lorem Ipsum has been the printing
                    industry's standard dummy text since the 1500s, when an
                    anonymous printer pieced together chunks of text to make a
                    specimen book of typefaces.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
