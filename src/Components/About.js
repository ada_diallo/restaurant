import React from "react";
import plat from "./Images/images.jpg"
import signature from "./Images/977d6ffb376c177e0139602d8b9c9e2e.jpg"
import TitleMenu from "./TitleMenu";

export default function About () {
    return(
        <div className="container-fluid contain-about d-flex justify-content-center align-items-center">
        <div className="container " id="about">
            <div className="row d-flex justify-content-center align-items-center pt-5" >
            <div className="col-lg-6  d-flex about-card d-flex   justify-content-center align-items-center">
                    <div className="text-center">
                    <h1 className="h1  ">A propos du restaurant</h1> 
                <p className="p pt-2 zoom">
                Lorem Ipsum is simply dummy text used in typesetting and layout before printing. Lorem Ipsum has been the printing industry's standard dummy text since the 1500s, when an anonymous printer pieced together chunks of text to make a specimen book of typefaces.
                </p>
                <img src={signature} className="img-fluid img-signature"/>

                    </div>
              
                </div>
                <div className="col-lg-6  d-flex about-card d-flex   justify-content-center align-items-center">
                    <div className="mt-5">
                    <div className="card zoom">
                        <img src={plat } className=' img-fluid'/>
                    </div>
                    <div className="card zoom">
                        <img src={plat } className=' img-fluid mt-3' />
                    </div>
                    </div>
                    <div className="m-3">
                    <div className="card zoom">
                        <img src={plat } className='img-fluid'/>
                    </div>
                    <div className="card zoom">
                        <img src={plat } className='img-fluid mt-3 '/>
                    </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
    )
}