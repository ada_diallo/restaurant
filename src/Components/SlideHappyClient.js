import React from "react";
import client from "./Images/client.jpg"

export default function SlideHappyClient () {
    return(
        <div className="container">
              <div className="d-flex flex-column justify-content-center align-items-center">
                <h3 className="h3 text-white">Client(es) Heureux(se)</h3>
                <p className="text-white p-slide">
                Lorem Ipsum is simply dummy text used in typesetting and
                    layout before printing. Lorem Ipsum has been the printing<br/>
                    industry's standard dummy text since the 1500s, when an
                    anonymous printer pieced together
                    <br/> chunks of text to make a
                    specimen book of typefaces.
                </p>
                <div className="text-center">
                    <img src={client} className='img-fluid img-client'/>
                    <h6 className="pt-3">Alice</h6>
                </div>


                </div>
        </div>
    )
}