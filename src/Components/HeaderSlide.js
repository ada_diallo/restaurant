import React from "react";
import { Carousel } from "react-bootstrap";
import Header from "./Header";
import Header2 from "./Header2";
import Header3 from "./Header3";


export default function HeaderSlide () {
    return(
        <div className="">
             <div className="row ">
          <div className="col-12 ">
            <Carousel   controls={false} wrape={false}
              renderIndicator={(onClickHandler, isSelected, index, label) => {
                const defStyle = { marginLeft: 20, color: "white", cursor: "pointer" };
                const style = isSelected
                  ? { ...defStyle, color: "red" }
                  : { ...defStyle };
                return (
                  <span
                    style={style}
                    onClick={onClickHandler}
                    onKeyDown={onClickHandler}
                    value={index}
                    key={index}
                    role="button"
                    tabIndex={0}
                    aria-label={`${label} ${index + 1}`}
                  >
                    {"cust " + index}
                  </span>
                );
              }}
            
            >
              <Carousel.Item >
              <div className="">
                <Header/>
              </div>
              </Carousel.Item>
              <Carousel.Item >
              <div className="">
                <Header2/>
              </div>
              </Carousel.Item>
              <Carousel.Item >
              <div className="">
                <Header3/>
              </div>
              </Carousel.Item>
              </Carousel>

        </div>
        </div>
        </div>
    )
}